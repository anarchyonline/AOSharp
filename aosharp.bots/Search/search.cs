﻿using AOSharp.Core;
using AOSharp.Core.UI;

using AOSharp.Common.GameData;
using System;
using System.Collections.Generic;
using DynelManager = AOSharp.Core.DynelManager;
using Vector3 = AOSharp.Common.GameData.Vector3;
using System.Linq;
using System.IO;
using NLog.Targets;
using Xamarin.Forms;
using System.Xml.Linq;
using NLog.LayoutRenderers;
using System.Runtime.Serialization.Json;

namespace Desu
{
    class ranges
    {
        public int level;
        public int min;
        public int max;
    }
    public class Search : AOPluginEntry
    {
        private string targetFile;
        private SimpleChar target;
        private bool attackPlayers;
        private int currentTarget = -1;
        private int nullitySphere2 = 150502;
        private int range = 220;

        private Dictionary<Profession, Vector3> ProfessionColors = new Dictionary<Profession, Vector3>
        {
            { Profession.Doctor , DebuggingColor.Red} ,
            { Profession.Trader , DebuggingColor.LightBlue} ,
            { Profession.Engineer , DebuggingColor.Green} ,
            { Profession.NanoTechnician , DebuggingColor.White} ,
            { Profession.Agent , DebuggingColor.Yellow} ,
            { Profession.MartialArtist , DebuggingColor.Purple} ,
            { Profession.Adventurer , DebuggingColor.White} ,
            { Profession.Enforcer , DebuggingColor.White} ,
            { Profession.Soldier , DebuggingColor.LightBlue} ,
            { Profession.Shade , DebuggingColor.White} ,
            { Profession.Keeper , DebuggingColor.White} ,
            { Profession.Bureaucrat , DebuggingColor.White} ,
            { Profession.Metaphysicist , DebuggingColor.White} ,
        };

        private static ranges[] pvpRanges =
        {
            new ranges{ level = 220, min = 175, max = 220 },
            new ranges{ level = 219, min = 174, max = 220 },
            new ranges{ level = 218, min = 174, max = 220 },
            new ranges{ level = 217, min = 173, max = 220 },
            new ranges{ level = 216, min = 172, max = 220 },
            new ranges{ level = 215, min = 171, max = 220 },
            new ranges{ level = 214, min = 170, max = 220 },
            new ranges{ level = 213, min = 170, max = 220 },
            new ranges{ level = 212, min = 169, max = 220 },
            new ranges{ level = 211, min = 168, max = 220 },
            new ranges{ level = 210, min = 167, max = 220 },
            new ranges{ level = 209, min = 166, max = 220 },
            new ranges{ level = 208, min = 166, max = 220 },
            new ranges{ level = 207, min = 165, max = 220 },
            new ranges{ level = 206, min = 164, max = 220 },
            new ranges{ level = 205, min = 163, max = 220 },
            new ranges{ level = 204, min = 162, max = 220 },
            new ranges{ level = 203, min = 162, max = 220 },
            new ranges{ level = 202, min = 161, max = 220 },
            new ranges{ level = 201, min = 160, max = 220 },
            new ranges{ level = 200, min = 159, max = 220 },
            new ranges{ level = 199, min = 158, max = 220 },
            new ranges{ level = 198, min = 158, max = 220 },
            new ranges{ level = 197, min = 157, max = 220 },
            new ranges{ level = 196, min = 156, max = 220 },
            new ranges{ level = 195, min = 155, max = 220 },
            new ranges{ level = 194, min = 154, max = 220 },
            new ranges{ level = 193, min = 154, max = 220 },
            new ranges{ level = 192, min = 153, max = 220 },
            new ranges{ level = 191, min = 152, max = 220 },
            new ranges{ level = 190, min = 151, max = 220 },
            new ranges{ level = 189, min = 150, max = 220 },
            new ranges{ level = 188, min = 150, max = 220 },
            new ranges{ level = 187, min = 149, max = 220 },
            new ranges{ level = 186, min = 148, max = 220 },
            new ranges{ level = 185, min = 147, max = 220 },
            new ranges{ level = 184, min = 146, max = 220 },
            new ranges{ level = 183, min = 146, max = 220 },
            new ranges{ level = 182, min = 145, max = 220 },
            new ranges{ level = 181, min = 144, max = 220 },
            new ranges{ level = 180, min = 143, max = 220 },
            new ranges{ level = 179, min = 142, max = 220 },
            new ranges{ level = 178, min = 142, max = 220 },
            new ranges{ level = 177, min = 141, max = 220 },
            new ranges{ level = 176, min = 140, max = 220 },
            new ranges{ level = 175, min = 139, max = 220 },
            new ranges{ level = 174, min = 138, max = 219 },
            new ranges{ level = 173, min = 138, max = 217 },
            new ranges{ level = 172, min = 137, max = 216 },
            new ranges{ level = 171, min = 136, max = 215 },
            new ranges{ level = 170, min = 135, max = 214 },
        };
        private List<string> _playersToHighlight = new List<string>();
        private string targetPath;
        private bool startSearching = false;
        public override void Run(string pluginDir)
        {
            try
            {
                targetPath = Environment.GetEnvironmentVariable("TARGET_PATH");
                if (targetPath == null)
                    targetPath = Directory.GetCurrentDirectory();

                targetFile = targetPath + "\\" + "targets.txt";

                if (File.Exists(targetFile))
                {
                    _playersToHighlight.AddRange(File.ReadAllLines(targetFile));
                }

                Chat.WriteLine("Search Loaded!", ChatColor.LightBlue);
            }
            catch (Exception e)
            {
                Chat.WriteLine("Run: " + e.Message);
            }
        }

        private void PrintAssistCommandUsage(ChatWindow chatWindow)
        {
            string help = "Usage:\n" +
                "/search \n" +
                "/target\n" +
                "/attack \n";
            //"/stop ";    

            chatWindow.WriteLine(help, ChatColor.LightBlue);
        }

        public Search()
        {
            Game.OnUpdate += OnUpdate;
            Game.TeleportEnded += OnZoned;
            Chat.RegisterCommand("search", SearchPlayers);
            Chat.RegisterCommand("target", TargetPlayers);
            Chat.RegisterCommand("attack", AttackPlayers);
            Chat.RegisterCommand("dickheads", Dickheads);
            Chat.RegisterCommand("enemies", Enemies);
        }

        (Profession, bool) GetProf(string name)
        {
            bool isProf = false;
            Profession prof;
            switch (name)
            {
                case "doc":
                case "doctor":
                case "doctors":
                case "docs":
                    isProf = true;
                    prof = Profession.Doctor;

                    break;
                case "crats":
                case "crat":
                case "bureaucrat":
                case "bureaucrats":
                    isProf = true;
                    prof = Profession.Bureaucrat;

                    break;
                case "sol":
                case "sols":
                case "soldier":
                case "soldiers":
                    isProf = true;
                    prof = Profession.Soldier;

                    break;
                case "trad":
                case "trads":
                case "traders":
                case "trader":
                    isProf = true;
                    prof = Profession.Trader;

                    break;
                case "agent":
                case "agents":
                    isProf = true;
                    prof = Profession.Agent;

                    break;
                case "nt":
                case "nts":
                    isProf = true;
                    prof = Profession.NanoTechnician;
                    break;
                case "mp":
                case "mps":
                    isProf = true;
                    prof = Profession.Metaphysicist;
                    break;
                case "engi":
                case "engis":
                case "engs":
                case "eng":
                case "engineers":
                case "engineer":
                    isProf = true;
                    prof = Profession.Engineer;

                    break;
                case "adv":
                case "advi":
                case "advis":
                    isProf = true;
                    prof = Profession.Adventurer;

                    break;
                case "enf":
                case "enfs":
                case "enfos":
                case "enforcer":
                case "enforcers":
                    isProf = true;
                    prof = Profession.Enforcer;
                    break;
                case "fix":
                case "fixers":
                case "fixer":
                    isProf = true;
                    prof = Profession.Fixer;
                    break;
                case "keep":
                case "keeper":
                case "keepers":
                    isProf = true;
                    prof = Profession.Keeper;
                    break;
                case "shade":
                case "shades":
                    isProf = true;
                    prof = Profession.Shade;
                    break;
                default:
                    isProf = false;
                    prof = Profession.Unknown;
                    break;


            }
            return (prof, isProf);
        }
        private void TargetPlayers(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (!DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                { 
                    SimpleChar []targets = DynelManager.Players
                    .Where(c => c.Side == Side.Neutral || c.Side == Side.OmniTek /*|| c.Side == Side.Clan*/)
                    .Where(c => {

                        ranges r = pvpRanges.Where(l => l.level == range).FirstOrDefault();
                        if (r == null)
                        {
                            r = pvpRanges.Where(l => l.level == 220).FirstOrDefault();
                        }

                        if (c.Level >= r.min && c.Level <= r.max)
                        {
                            return true;
                        }
                        else
                            return false;

                    })
                    .Where(p =>
                    {
                        if (p.Buffs.Find(nullitySphere2, out Buff buff) && buff.RemainingTime > 5)
                            return false;
                        else
                            return true;
                    })
                    .Where(c => c.IsPlayer)
                    .OrderBy(c => c.IsPlayer)
                    .ThenByDescending(c => c.IsInAttackRange())
                    .ThenBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .ThenByDescending(c => c.HealthPercent < 75)
                    .ThenBy(c => c.Health)
                    .ToArray();

                    if (targets != null && targets.Length > 0)
                    {
                        currentTarget++;

                        if (currentTarget >= targets.Length)
                        {
                            currentTarget = 0;
                        }
                        if (Targeting.Target != null && Targeting.Target.Name == targets[currentTarget].Name)
                        {
                            currentTarget++;
                            if (currentTarget >= targets.Length)
                            {
                                currentTarget = 0;
                            }
                        }

                        if (targets[currentTarget].Name == DynelManager.LocalPlayer.Name)
                        {
                            currentTarget++;
                            if (currentTarget >= targets.Length)
                            {
                                currentTarget = 0;
                            }
                        }

                        //Chat.WriteLine("Target: " + targets[currentTarget].Name, ChatColor.LightBlue);
                        Targeting.SetTarget(targets[currentTarget]);
                        target = targets[currentTarget];
                        if (attackPlayers && targets[currentTarget].IsInAttackRange() && targets[currentTarget].IsInLineOfSight)
                        {
                            Chat.WriteLine("Attacking target: " + targets[currentTarget].Name, ChatColor.Red);
                            DynelManager.LocalPlayer.Attack(targets[currentTarget]);
                        }
                    }
                }           
            }
            catch (Exception e)
            {
                Chat.WriteLine("TargetPlayers: " + e.Message);
                if (DynelManager.LocalPlayer.IsAttacking) 
                    DynelManager.LocalPlayer.StopAttack();
                currentTarget = -1;
            }
        }

        private void AttackPlayers(string command, string[] args, ChatWindow chatWindow)
        {
            try
            {
                if (args.Length > 0)
                {
                    switch (args[0].ToLower())
                    {
                        case "enable":
                        case "on":
                        case "start":
                            attackPlayers = true;
                            Chat.WriteLine("Attacking players enabled", ChatColor.Green);

                            break;
                        case "disable":
                        case "off":
                        case "stop":
                        case "s":
                        default:
                            attackPlayers = false;
                            Chat.WriteLine("Attacking players disabled", ChatColor.Gold);
                            break;

                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine("AttackPlayers: " + e.Message);
            }
        }
        private void SearchPlayers(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    startSearching = true;
                    Chat.WriteLine("Searching players enabled");

                    return;
                }
                else
                {
                    string name = param[0].ToLower();
                    switch (name)
                    {
                        case "start":
                        case "on":
                            startSearching = true;
                            Chat.WriteLine("Searching players enabled");
                            break;

                        case "stop":
                        case "off":
                            startSearching = false;
                            Chat.WriteLine("Searching players disabled");

                            break;
                        case "range":
                            int r = param[1] != null ? Int32.Parse(param[1]) : 220;
                            range = r;
                            break;
                        default:
                            startSearching = true;

                            if (name == DynelManager.LocalPlayer.Name.ToLower())
                            {
                                return;
                            }
                            bool isProf;
                            Profession prof;

                            (prof, isProf) = GetProf(name);

                            foreach (SimpleChar p in DynelManager.Players)
                            {
                                /*if (isProf == true)
                                {
                                    if (p.Profession == prof )//&& p.Side != Side.Clan)
                                    {
                                        Chat.WriteLine($"Searching for: " + p.Name + ", " + p.Profession, ChatColor.Gold);
                                        _playersToHighlight.Add(p.Name);
                                        Targeting.SetTarget(p);
                                        return;
                                    }

                                }
                                else
                                {*/

                                    if (p.Name.ToLower().Contains(name))
                                    {
                                        if (!_playersToHighlight.Contains(p.Name))
                                        {
                                            _playersToHighlight.Add(p.Name);
                                            Chat.WriteLine("Adding " + p.Name + " to search list, and updating file " + targetFile);
                                            File.WriteAllLines(targetFile, _playersToHighlight);
                                        }
                                        Targeting.SetTarget(p);
                                        return;
                                    }
                                //}
                            }
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine("SearchPlayers: " + e.Message);
            }
        }

        private void Dickheads(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                foreach (string dickhead in _playersToHighlight)
                {
                    foreach (SimpleChar p in DynelManager.Players)
                    {
                        if (p.Name.Contains(dickhead))
                        {
                            Chat.WriteLine(p.Name + ", " + p.Profession);
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine("Dickheads: " + e.Message);
            }
        }

        private void Enemies(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                int counter = 0;
                int omnis = 0;
                int neuts = 0;
                foreach (SimpleChar p in DynelManager.Players)
                {
                    if (p.IsPlayer && (p.Side == Side.Neutral || p.Side == Side.OmniTek))
                    {
                        counter++;

                        if (p.Side == Side.OmniTek)
                            omnis++;
                        if (p.Side == Side.Neutral)
                            neuts++;
                    }
                }
                Chat.WriteLine("Enemies: " + counter + ",   Omnis: " + omnis + ",   Neuts: " + neuts);
            }
            catch (Exception e)
            {
                Chat.WriteLine("Enemies: " + e.Message);
            }
        }


        private void DrawPlayer(SimpleChar player)
        {
            try
            {
                if (player != null)
                {
                    Vector3 color = DebuggingColor.Blue;
                    if (player.Side == Side.Neutral)
                        color = DebuggingColor.Purple;

                    if (target != null && player.Name == target.Name)
                        color = DebuggingColor.Green;
                    Debug.DrawSphere(player.Position, 1, color);
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, player.Position, color);
                }
                else
                {
                    return;
                }
            }

            catch (Exception e)
            {
                Chat.WriteLine("DrawPlayers: " + e.Message);
            }
        }
        private void DrawFoundPlayers()
        {
            try
            {
                if (startSearching == true)
                {
                    foreach (SimpleChar p in DynelManager.Players)
                    {
                        if (/*p.Side == Side.Clan||*/ p.Side == Side.OmniTek || p.Side == Side.Neutral)
                        {
                            DrawPlayer(p);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine("DrawFoundPlayers: " + e.Message);
            }
        }
        private void OnZoned(object s, EventArgs e)
        {
            DynelManager.LocalPlayer.StopAttack();
            currentTarget = -1;
            attackPlayers = false;
        }
        private void OnUpdate(object sender, float e)
        {
            DrawFoundPlayers();

            if (attackPlayers == true && !DynelManager.LocalPlayer.IsAttacking)
            {
                TargetPlayers("", null, null);
            }
            if (DynelManager.LocalPlayer.IsAttacking)
            {
                SimpleChar t = DynelManager.Players.Where( p => Targeting.Target.Name == p.Name).FirstOrDefault();
                if (t != null && ((t.Buffs.Find(nullitySphere2, out Buff buff) && buff.RemainingTime > 5) ||
                    !t.IsInAttackRange() ||
                    !t.IsInLineOfSight
                   ))
                {
                    DynelManager.LocalPlayer.StopAttack();
                }
            }
        }
    }
}
