﻿using System;
using System.Diagnostics;
using AOSharp.Core;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Core.UI.Options;
using AOSharp.Core.Combat;
using AOSharp.Common;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using MultiboxHelper.IPCMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Core.Inventory;
using System.Security.Cryptography;
using WindowsInput.Native;
using WindowsInput;
using System.Threading;
using System.Threading.Tasks;

namespace MultiboxHelper
{
    public class MultiboxHelper : AOPluginEntry
    {
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        private int FountainOfLife = 302907;
        private int currentHealer = -1;
        private IPCChannel IPCChannel;
        private bool syncMoves = true;
        private bool syncAttacks = true;
        private bool syncUse = true;
        private int floor;
        private bool monitorPOHBuffs = false;
        private bool needGreenLight = false;
        private bool monitor12manDebuff = false;
        private bool usePandeRing = false;
        private bool useRandomPos = false;

        private bool testMeepToLocation1 = false;
        private bool testGoToLocation1 = false;
        private bool testGoToLocation2 = false;
        private bool testGoToLocation3 = false;

        private bool positionProfsForFloor5 = false;

        private Vector3 location1 = new Vector3(615.5, 310.3, 513.7);
        private Vector3 location1meep = new Vector3(614.5, 310.3, 514.7);
        private Vector3 location2 = new Vector3(644.5, 310.9, 502.9);
        private Vector3 location3 = new Vector3(663.9, 310.2, 517.5);
        private const float maxDistance = 0.6F;


        // Keeper commands
        private bool castAntifear = false;

        MovementController movementController = new MovementController(true);
        private Vector3 floor2_support_pos = new Vector3(312.0, 1.5, 335.5);
        private Vector3 floor2_tank_pos = new Vector3(319.0, 1.5, 366.5);

        private Vector3 startpoint_12man_yellow = new Vector3(101.8, 29.0, 61.9);
        private Vector3 startpoint_12man_red = new Vector3(75.6, 29.0, 61.4);

        private Vector3 red_step1_12man = new Vector3(42.1, 29.0, 57.2);
        private Vector3 red_step2_12man = new Vector3(35.9, 29.0, 46.3);
        private Vector3 red_pedestal_12man = new Vector3(35.6, 29.3, 30.0);

        private Vector3 yellow_step1_12man = new Vector3(149.7, 29.0, 60.2);
        private Vector3 yellow_step2_12man = new Vector3(163.9, 29.0, 43.3);
        private Vector3 yellow_pedestal_12man = new Vector3(164.2, 29.3, 30.4);

        private Vector3 fight_position_12man1 = new Vector3(97.4, 29.0, 58.9);
        private Vector3 fight_position_12man1Red = new Vector3(68.0, 29.0, 61.7);
        private Vector3 fight_position_12man2 = new Vector3(77.6, 29.0, 26.0);

        private bool goToStartPointYellow = false;
        private bool goToStartPointRed = false;
        private bool goToYellow1 = false;
        private bool goToYellow2 = false;
        private bool goToYellowPlat = false;

        private bool goToRed1 = false;
        private bool goToRed2 = false;
        private bool goToRedPlat = false;
        private bool goBack12Man1Yellow = false;
        private bool goBack12Man1Red = false;
        private bool goBack12Man2 = false;
        public enum EngiDebufAuras
        {
            none,
            NSD,
            blinds
        }
        public enum KeeperCommands
        {
            antifear,
            stop
        }
        public enum PetCommands
        {
            wait,
            follow,
            warp
        }
        public enum RandomCommands
        {
            on,
            off
        }
        public enum DocCommands
        {
            CH
        }
        public enum InventoryItems
        {
            None,
            InsuranceClaimRecallBeacon
        }


        private bool IsActiveWindow => GetForegroundWindow() == Process.GetCurrentProcess().MainWindowHandle;

        public override void Run(string pluginDir)
        {
            IPCChannel = new IPCChannel(111);
            IPCChannel.RegisterCallback((int)IPCOpcode.Move, OnMoveMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Target, OnTargetMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Attack, OnAttackMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Heal, OnHealMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.StopAttack, OnStopAttackMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Use, OnUseMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.SyncMoves, OnSyncMovesMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.SyncAttacks, OnSyncAttacksMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Engi, OnEngiMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Keeper, OnKeeperMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Pet, OnPetMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.SocialAction, OnSocialActionMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Random, OnRandomMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Doc, OnDocMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Floor, OnFloorMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Test, OnTestMessage);


            //_menu = new Menu("MultiboxHelper", "MultiboxHelper");
            //_menu.AddItem(new MenuBool("SyncMove", "Sync Movement", true));
            //_menu.AddItem(new MenuBool("SyncAttack", "Sync Attacks", true));
            //_menu.AddItem(new MenuBool("SyncUse", "Sync Use", true));
            //OptionPanel.AddMenu(_menu);

            Network.N3MessageSent += Network_N3MessageSent;

            Chat.RegisterCommand("syncmoves", SyncMovesCommand);
            Chat.RegisterCommand("sm", SyncMovesCommand);
            Chat.RegisterCommand("syncattacks", SyncAttacksCommand);
            Chat.RegisterCommand("sa", SyncAttacksCommand);
            Chat.RegisterCommand("engi", EngiCommand);
            Chat.RegisterCommand("heal", HealFountainOfLifeAsync);
            Chat.RegisterCommand("12man", AntiFearCommand);
            Chat.RegisterCommand("fear", AntiFearCommand);
            Chat.RegisterCommand("doc", DocCommand);
            Chat.RegisterCommand("poh", FloorCommand);
            Chat.RegisterCommand("floor", FloorCommand);
            Chat.RegisterCommand("test", TestCommand);
            Chat.RegisterCommand("pets", PetCommand);
            Chat.RegisterCommand("random", RandomCommand);
            Chat.RegisterCommand("rpos", RandomCommand);

            Game.TeleportEnded += OnZoned;
            Game.OnUpdate += OnUpdate;

            Chat.WriteLine("Multibox Helper Loaded! V1.4");
        }

        private void TestCommand(string command, string[] param, ChatWindow chatWindow)
        {
            goToStartPointYellow = true;
            //testMeepToLocation1 = true;
            return;
            try
            {
                Vector3 pos = new Vector3(639.9, 36.4, 1555.8);

                if (param.Length == 4)
                {
                    pos = new Vector3(float.Parse(param[1]), float.Parse(param[2]), float.Parse(param[3]));
                }
                IPCChannel.Broadcast(new TestMessage()
                {
                    PlayfieldId = Playfield.Identity.Instance,
                    Position = pos
                });
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
        private void RandomCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                RandomCommands randomCommand;
                if (param.Length < 1)
                {
                    randomCommand = RandomCommands.on;
                }
                else
                {
                    switch (param[0].ToLower())
                    {
                        default:
                        case "on":
                            randomCommand = RandomCommands.on;
                            break;
                        case "off":
                            randomCommand = RandomCommands.off;
                            break;
                    }
                }
                Chat.WriteLine($"Random pos set to: {randomCommand.ToString()}");
                IPCChannel.Broadcast(new RandomMessage()
                {
                    command = (int)randomCommand
                });
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void PetCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                PetCommands petCommand;
                if (param.Length < 1)
                {
                    petCommand = PetCommands.follow;
                }
                else
                {
                    switch (param[0].ToLower())
                    {
                        case "wait":
                            petCommand = PetCommands.wait;
                            break;
                        case "warp":
                            petCommand = PetCommands.warp;
                            break;
                        default:
                        case "follow":
                            petCommand = PetCommands.follow;
                            break;
                    }
                }
                IPCChannel.Broadcast(new PetMessage()
                {
                    command = (int)petCommand
                });
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
        private void AntiFearCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                int keeperCommand = (int)KeeperCommands.antifear;

                if (param.Length > 0)
                {
                    keeperCommand = (int)KeeperCommands.stop;
                    resetMovementVars();
                }
                else
                {
                    DocCommand(command, param, chatWindow);

                    IPCChannel.Broadcast(new KeeperMessage()
                    {
                        command = keeperCommand
                    });
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
        private void FloorCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {

                if (param.Length < 1)
                {
                    Chat.WriteLine("Usage: /floor <floor>");
                    Chat.WriteLine("Example: /floor 3");
                    return;
                }
                floor = int.Parse(param[0]);

                if (floor != 5)
                {
                    syncMoves = false;
                    syncAttacks = false;
                }
                else
                {
                    syncMoves = false;
                    syncAttacks = true;
                }
                FloorMessage fm = new FloorMessage()
                {
                    Floor = floor
                };

                IPCChannel.Broadcast(fm);


                switch (floor)
                {
                    case 2:
                    case 5:
                        OnFloorMessage(0, fm);

                        break;

                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }

        }

        private void SyncMovesCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                string commandParam;
                if (param.Length < 1)
                    commandParam = "true";
                else
                    commandParam = param[0].ToLower();

                switch (commandParam)
                {
                    case "true":
                    case "start":
                    case "on":
                        Chat.WriteLine("Start syncing moves...");
                        syncMoves = true;
                        break;
                    case "false":
                    case "stop":
                    case "off":
                        Chat.WriteLine("Stop syncing moves...");
                        syncMoves = false;
                        break;
                }
                IPCChannel.Broadcast(new SyncMovesMessage()
                {
                    syncMoves = syncMoves
                });
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void SyncAttacksCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {

                string commandParam;
                if (param.Length < 1)
                    commandParam = "true";
                else
                    commandParam = param[0].ToLower();

                switch (commandParam)
                {
                    case "true":
                    case "start":
                    case "on":
                        Chat.WriteLine("Start syncing attacks...");
                        syncAttacks = true;
                        break;
                    case "false":
                    case "stop":
                    case "off":
                        Chat.WriteLine("Stop syncing attacks...");
                        syncAttacks = false;
                        break;

                }
                IPCChannel.Broadcast(new SyncAttacksMessage()
                {
                    syncAttacks = syncAttacks
                });
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
        private void DocCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {

                string commandParam;
                if (param.Length < 1)
                    commandParam = "ch";
                else
                    commandParam = param[0].ToLower();

                DocCommands docCommands;
                switch (commandParam)
                {
                    default:
                    case "ch":
                        Chat.WriteLine("Doc will cast Alpha Omega...");
                        docCommands = DocCommands.CH;
                        break;
                }
                IPCChannel.Broadcast(new DocMessage()
                {
                    buff = (int)docCommands
                });

            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
        private void EngiCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {

                string commandParam;
                if (param.Length < 1)
                    commandParam = "none";
                else
                    commandParam = param[0].ToLower();
                EngiDebufAuras engiAura;
                switch (commandParam)
                {
                    case "nsd":
                        Chat.WriteLine("Engi will cast NSD...");
                        engiAura = EngiDebufAuras.NSD;
                        break;
                    case "blinds":
                        Chat.WriteLine("Engi will cast blinds...");
                        engiAura = EngiDebufAuras.blinds;
                        break;
                    default:
                    case "none":

                        Chat.WriteLine("Engi will remove blinds and NSD...");
                        engiAura = EngiDebufAuras.none;
                        break;

                }
                IPCChannel.Broadcast(new EngiMessage()
                {
                    buff = (int)engiAura
                });

            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private async void HealFountainOfLifeAsync(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (!Team.IsInTeam || !IsActiveWindow)
                    return;

                foreach (TeamMember member in Team.Members)
                {
                    if (member.Name != DynelManager.LocalPlayer.Name)
                    {
                        Chat.WriteLine("Healer: " + member.Name);


                        IPCChannel.Broadcast(new HealMessage()
                        {
                            Instance = member.Character.Identity.Instance,
                        });
                        await Task.Delay(500);
                        SimpleChar dyingTeamMember = DynelManager.Characters
                        .Where(c => c.IsAlive)
                        .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                        .Where(c => c.HealthPercent < 30)
                        .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                        .FirstOrDefault();

                        if (dyingTeamMember != null)
                        {
                            continue;
                        }
                        else
                            return;
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
        private void Network_N3MessageSent(object s, N3Message n3Msg)
        {
            if (n3Msg.N3MessageType == N3MessageType.SocialActionCmd)
            {
                SocialActionCmdMessage socialActionMsg = (SocialActionCmdMessage)n3Msg;
                IPCChannel.Broadcast(new SocialActionMessage()
                {
                    action = (int) socialActionMsg.Action
                });
            }
            if (n3Msg.N3MessageType == N3MessageType.Attack)
            {
                //If this is an attack only the team leader will issue the command
                //if (!Team.IsInTeam || !Team.IsLeader)
                if (!Team.IsInTeam || !IsActiveWindow)
                    return;

            }
            else
            {
                //Only the active window will issue commands
                if (!Team.IsInTeam || !IsActiveWindow)
                    return;
            }


            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity)
                return;

            if (n3Msg.N3MessageType == N3MessageType.CharDCMove)
            {
                if (!syncMoves)
                    return;

                CharDCMoveMessage charDCMoveMsg = (CharDCMoveMessage)n3Msg;
                IPCChannel.Broadcast(new MoveMessage()
                {
                    MoveType = charDCMoveMsg.MoveType,
                    PlayfieldId = Playfield.Identity.Instance,
                    Position = charDCMoveMsg.Position,
                    Rotation = charDCMoveMsg.Heading
                });

            }
            else if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
            {
                if (!syncMoves)
                    return;

                CharacterActionMessage charActionMsg = (CharacterActionMessage)n3Msg;

                if (charActionMsg.Action != CharacterActionType.StandUp)
                    return;

                IPCChannel.Broadcast(new MoveMessage()
                {
                    MoveType = MovementAction.LeaveSit,
                    PlayfieldId = Playfield.Identity.Instance,
                    Position = DynelManager.LocalPlayer.Position,
                    Rotation = DynelManager.LocalPlayer.Rotation
                });
            }
            else if (n3Msg.N3MessageType == N3MessageType.LookAt)
            {
                LookAtMessage lookAtMsg = (LookAtMessage)n3Msg;
                IPCChannel.Broadcast(new TargetMessage()
                {
                    Target = lookAtMsg.Target
                });
            }
            else if (n3Msg.N3MessageType == N3MessageType.Attack)
            {
                if (!syncAttacks)
                    return;

                AttackMessage attackMsg = (AttackMessage)n3Msg;
                IPCChannel.Broadcast(new AttackIPCMessage()
                {
                    Target = attackMsg.Target,
                    PlayfieldId = Playfield.Identity.Instance
                });
            }
            else if (n3Msg.N3MessageType == N3MessageType.StopFight)
            {
                if (!syncAttacks)
                    return;

                StopFightMessage lookAtMsg = (StopFightMessage)n3Msg;
                IPCChannel.Broadcast(new StopAttackIPCMessage());
            }
            else if (n3Msg.N3MessageType == N3MessageType.GenericCmd)
            {
                if (!syncUse) //!_menu.GetBool("SyncUse"))
                    return;

                GenericCmdMessage genericCmdMsg = (GenericCmdMessage)n3Msg;
                Chat.WriteLine("Using " + genericCmdMsg.Target.Type.ToString());


                List<Item> characterItems = Inventory.Items;

                foreach (Item item in characterItems)
                {
                    if (item.Slot == genericCmdMsg.Target && item.Name == "Insurance Claim Recall Beacon")
                    {
                        Chat.WriteLine($"{item.Slot} - {item.LowId} - {item.Name} - {item.QualityLevel} - {item.UniqueIdentity}");
                        IPCChannel.Broadcast(new UseMessage()
                        {
                            item = (int)InventoryItems.InsuranceClaimRecallBeacon
                        });
                        return;
                    }
                }
                List<Container> backpacks = Inventory.Backpacks;
                foreach (Container backpack in backpacks)
                {
                    foreach (Item i in backpack.Items)
                    {
                        if (i.Name == "Insurance Claim Recall Beacon")
                        {
                            // insurance item
                            int a = 1;
                            Chat.WriteLine($"{backpack.Identity} - Item: {i.Name}   identity: {i.UniqueIdentity}");

                        }

                    }
                }
                SimpleItem simpleItem = DynelManager.GetDynel<SimpleItem>(genericCmdMsg.Target);
                if (genericCmdMsg.Action == GenericCmdAction.Use &&
                    (genericCmdMsg.Target.Type == IdentityType.Terminal)
                    /*                    || 
                                            (
                                                genericCmdMsg.Target.Name == "Keys" &&
                                                genericCmdMsg.Target.Type == IdentityType.Backpack
                                            )*/
                    )
                {




                    //Chat.WriteLine("Using " + );
                    UseMessage msg = new UseMessage()
                    {
                        Target = genericCmdMsg.Target,
                        item = (int)InventoryItems.None
                    };
                    IPCChannel.Broadcast(msg);

                    usePandeRing = true;
                }
            }
            
        }
    
        private void OnTestMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning)
                return;

            TestMessage testMsg = (TestMessage)msg;

            if (Playfield.Identity.Instance != testMsg.PlayfieldId)
                return;
            DynelManager.LocalPlayer.Position = testMsg.Position;

        }
        private void OnMoveMessage(int sender, IPCMessage msg)
        {
            //Only followers will act on commands
            if (!Team.IsInTeam || IsActiveWindow)
                return;

            if (Game.IsZoning)
                return;

            MoveMessage moveMsg = (MoveMessage)msg;

            if (Playfield.Identity.Instance != moveMsg.PlayfieldId)
                return;

            if (useRandomPos == true)
            {
                int entropy = 2;
                Vector3 randomHoldPos = moveMsg.Position;
                
                switch (moveMsg.MoveType)
                {
                    case MovementAction.TurnRightStart:
                    case MovementAction.TurnRightMouse:
                    case MovementAction.TurnRightStop:
                    case MovementAction.TurnLeftStart:
                    case MovementAction.TurnLeftMouse:
                    case MovementAction.TurnLeftStop:

                        DynelManager.LocalPlayer.Rotation = moveMsg.Rotation;
                        break;

                    default:

                        randomHoldPos.X += Utils.Next(-entropy, entropy);
                        randomHoldPos.Z += Utils.Next(-entropy, entropy);

                        DynelManager.LocalPlayer.Position = randomHoldPos;

                        break;
                }
            }
            else
            {
                DynelManager.LocalPlayer.Rotation = moveMsg.Rotation;
                DynelManager.LocalPlayer.Position = moveMsg.Position;
            }

            MovementController.Instance.SetMovement(moveMsg.MoveType);
        }

        private void OnTargetMessage(int sender, IPCMessage msg)
        {
            //if (!Team.IsInTeam || IsActiveWindow)
            //    return;

            if (Game.IsZoning)
                return;

            TargetMessage targetMsg = (TargetMessage)msg;
            Targeting.SetTarget(targetMsg.Target);
        }

        private void OnAttackMessage(int sender, IPCMessage msg)
        {
            //if (!Team.IsInTeam || IsActiveWindow)
              //  return;

            if (Game.IsZoning)
                return;

            AttackIPCMessage attackMsg = (AttackIPCMessage)msg;


            if (Playfield.Identity.Instance != attackMsg.PlayfieldId)
                return;

            // Verify that this isn't "Azdaja the Joyous", if it is, and you are not soldja or enfo, 
            // and its health is not 20%, do not attack

            foreach (SimpleChar npc in DynelManager.Characters)
            {
                if (npc.Identity == attackMsg.Target)
                {
                    if (npc.Name == "Azdaja the Joyous")
                    {

                        if (npc.HealthPercent > 20.0 &&
                            (
                             DynelManager.LocalPlayer.Profession != Profession.Enforcer &&
                             DynelManager.LocalPlayer.Profession != Profession.Soldier 
                            )
                           )
                        {
                            return;
                        }
                    }
                }
            }

            DynelManager.LocalPlayer.Attack(attackMsg.Target);
            DynelManager.LocalPlayer.Pets.Attack(attackMsg.Target);
           
        }
        private void OnHealMessage(int sender, IPCMessage msg)
        {
            if (!Team.IsInTeam)
              return;

            if (Game.IsZoning)
                return;

            HealMessage healMsg = (HealMessage)msg;


            //if (Playfield.Identity.Instance != healMsg.PlayfieldId)
            //    return;

            Chat.WriteLine("Healer: " + healMsg.Instance, ChatColor.Orange);
            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam() && healMsg.Instance == DynelManager.LocalPlayer.Identity.Instance)
            {
                Chat.WriteLine("IT'S ME!", ChatColor.Orange);
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.IsAlive)
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent < 30)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (dyingTeamMember != null)
                {
                    Spell.Find(FountainOfLife, out Spell spell);
                    if (spell != null)
                    {
                        Chat.WriteLine("Casting ICH to " + dyingTeamMember.Name, ChatColor.Orange);
                        spell.Cast(dyingTeamMember, true);
                    }
                }
            }
        }
        private void OnStopAttackMessage(int sender, IPCMessage msg)
        {
            if (!Team.IsInTeam || IsActiveWindow)
                return;

            if (Game.IsZoning)
                return;

            DynelManager.LocalPlayer.StopAttack();
        }

        private void OnUseMessage(int sender, IPCMessage msg)
        {
            if (!Team.IsInTeam || IsActiveWindow)
                return;

            if (Game.IsZoning)
                return;


            UseMessage useMsg = (UseMessage)msg;

            if (useMsg.item == (int)InventoryItems.None)
            {
                
                if (Targeting.Target != null && Targeting.Target.Name == "Pandemonium Portal")
                {
                    List<Item> characterItems = Inventory.Items;
                    foreach (Item item in characterItems)
                    {
                        if (item.Name.StartsWith("Pure Novictum Ring for the")) 
                        {
                            Chat.WriteLine($"Using {item.Name}");
                            item.UseOn(Targeting.Target.Identity);
                            return;
                        }
                    }
                }
                else
                {
                    DynelManager.GetDynel<SimpleItem>(useMsg.Target)?.Use();
                }
            }
            else if (useMsg.item == (int)InventoryItems.InsuranceClaimRecallBeacon)
            {
                List<Item> characterItems = Inventory.Items;
                foreach (Item item in characterItems)
                {
                    if (item.Name == "Insurance Claim Recall Beacon")
                    {
                        Chat.WriteLine($"Using {item.Name}");
                        item.Use();
                        return;
                    }
                }
            }
            
        }

        private void OnSyncMovesMessage(int sender, IPCMessage msg)
        {
            if (!Team.IsInTeam || IsActiveWindow)
                return;

            if (Game.IsZoning)
                return;

            SyncMovesMessage smMsg = (SyncMovesMessage)msg;
            syncMoves = smMsg.syncMoves;
        }
        private void OnSyncAttacksMessage(int sender, IPCMessage msg)
        {
            if (!Team.IsInTeam || IsActiveWindow)
                return;

            if (Game.IsZoning)
                return;

            SyncAttacksMessage saMsg = (SyncAttacksMessage)msg;
            syncAttacks = saMsg.syncAttacks;
        }
        
        private void OnDocMessage(int sender, IPCMessage msg)
        {

            if (Game.IsZoning)
                return;

            if (DynelManager.LocalPlayer.Profession == Profession.Doctor)
            {
                DocMessage docMsg = (DocMessage)msg;
                DocCommands requestedBuff = (DocCommands)docMsg.buff;

                if (requestedBuff == DocCommands.CH)
                {
                    //Alpha Omega nano ID is 42409 https://aoitems.com/item/42409/alpha-and-omega/

                    Spell.Find(42409, out Spell curSpell);
                    if (curSpell != null)
                    {
                        curSpell.Cast();
                    }
                }
            }
        }

        private void DancekeyPress(string command, string[] param, ChatWindow chatWindow)
        {
            InputSimulator sim = new InputSimulator();

            string danceCommand = param[0].ToLower();
            try
            {


                if (param.Length < 1)
                {
                    Chat.WriteLine($"Invalid command ", ChatColor.Yellow);
                    return;
                }


                if (param.Length == 1)
                {


                    switch (danceCommand)
                    {
                        default:
                        case "on":
                            Chat.WriteLine("Dance Mode Enabled", ChatColor.Green);
                            sim.Keyboard.KeyPress(VirtualKeyCode.NUMPAD1);

                            break;

                    }

                    IPCChannel.Broadcast(new DanceMessage()
                    {
                        dance = sim
                    }); ;

                }

            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void OnEngiMessage(int sender, IPCMessage msg)
        {

            if (Game.IsZoning)
                return;

            EngiMessage engiMsg = (EngiMessage)msg;
            EngiDebufAuras requestedBuff = (EngiDebufAuras)engiMsg.buff;

            if (DynelManager.LocalPlayer.Profession == Profession.Engineer)
            {

                // Since NSD is the top of the stacking chain, if it's requested, it will remove other nanos.
                // However if blinds are requested and NSD is running, we need to remove it
                if (requestedBuff == EngiDebufAuras.NSD)
                {
                    //NSD nano ID is 154725 https://aoitems.com/item/154725/null-space-disruptor/

                    Spell.Find(154725, out Spell curSpell);
                    if (curSpell != null)
                    {
                        curSpell.Cast();
                    }
                }
                else if (requestedBuff == EngiDebufAuras.blinds)
                {

                    if (DynelManager.LocalPlayer.Buffs.Find(NanoLine.EngineerDebuffAuras, out Buff buff))
                    {
                        if (buff.Name == "Null Space Disruptor")
                            buff.Remove();
                    }
                    //Disruptive Void Projector nano ID is 154715 https://aoitems.com/item/154715/disruptive-void-projector/
                    Spell.Find(154715, out Spell curSpell);
                    if (curSpell != null)
                    {
                        curSpell.Cast();
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Buffs.Find(NanoLine.EngineerDebuffAuras, out Buff buff))
                    {
                        buff.Remove();
                    }
                }
            }
            else if (DynelManager.LocalPlayer.Profession == Profession.Bureaucrat && requestedBuff != EngiDebufAuras.none)
            {
                //Demotivational Speech: Dead Man Walking https://aoitems.com/item/275826/demotivational-speech-dead-man-walking/
                Spell.Find(275826, out Spell curSpell);
                if (curSpell != null)
                {
                    curSpell.Cast();
                    curSpell.Cast();
                }
            }
        }
        private void resetMovementVars()
        {
            monitor12manDebuff = false;
            goToStartPointYellow = false;
            goToStartPointRed = false;
            goToYellow1 = false;
            goToYellow2 = false;
            goToYellowPlat = false;
            goToRed1 = false;
            goToRed2 = false;
            goToRedPlat = false;
            goBack12Man1Yellow = false;
            goBack12Man1Red = false;
            goBack12Man2 = false;
        }
        private void OnKeeperMessage(int sender, IPCMessage msg)
        {

            if (Game.IsZoning)
                return;

            KeeperMessage keepMsg = (KeeperMessage)msg;
            KeeperCommands keeperCommand = (KeeperCommands)keepMsg.command;

            if (keeperCommand == KeeperCommands.antifear)            
                monitor12manDebuff = true;
            else
            {
                resetMovementVars();
            }


            if (DynelManager.LocalPlayer.Profession == Profession.Keeper)
            {

                // Since NSD is the top of the stacking chain, if it's requested, it will remove other nanos.
                // However if blinds are requested and NSD is running, we need to remove it
                if (keeperCommand == KeeperCommands.antifear)
                {
                    castAntifear = true;
                }
                else // stop
                {
                    castAntifear = false;
                }
            }
        }
        private void OnSocialActionMessage(int sender, IPCMessage msg)
        {

            if (Game.IsZoning)
                return;

            SocialActionMessage socialMsg = (SocialActionMessage)msg;
            SocialAction saCommand = (SocialAction)socialMsg.action;

            InputSimulator sim = new InputSimulator();
            Chat.WriteLine($"Dance time");
            sim.Keyboard.KeyPress(VirtualKeyCode.F8);
            

        }


        private void OnPetMessage(int sender, IPCMessage msg)
        {

            if (Game.IsZoning)
                return;


            if (DynelManager.LocalPlayer.Pets.Length > 0)
            {
                
                PetMessage petMsg = (PetMessage)msg;
                PetCommands petCommand = (PetCommands)petMsg.command;

                // Since NSD is the top of the stacking chain, if it's requested, it will remove other nanos.
                // However if blinds are requested and NSD is running, we need to remove it
                if (petCommand == PetCommands.follow)
                {
                    DynelManager.LocalPlayer.Pets.Follow();
                }
                else if (petCommand == PetCommands.wait)
                {
                    DynelManager.LocalPlayer.Pets.Wait();
                }
                else if (petCommand == PetCommands.warp)
                {
                    // Pet Warp https://auno.org/ao/db.php?id=209488
                    Spell.Find(209488, out Spell curSpell);
                    if (curSpell != null)
                    {
                        curSpell.Cast();
                    }
                }
            }
        }

        private void OnRandomMessage(int sender, IPCMessage msg)
        {

            if (Game.IsZoning)
                return;


            RandomMessage randomMsg = (RandomMessage)msg;
            RandomCommands randomCommand = (RandomCommands)randomMsg.command;

            if (randomCommand == RandomCommands.off)
            {
                useRandomPos = false;
            }
            else
            {
                useRandomPos = true;
            }
        }


        private bool IsAttackingTeamMember()
        {
            foreach (TeamMember m in Team.Members)
            {

                if (DynelManager.LocalPlayer != null && DynelManager.LocalPlayer.FightingTarget != null &&
                    DynelManager.LocalPlayer.FightingTarget.Name == m.Character.Name)
                /*                DynelManager.LocalPlayer.FightingTarget.Name != "Deranged Xan" &&
                                DynelManager.LocalPlayer.FightingTarget.Name != "Left Hand of Insanity" &&
                                DynelManager.LocalPlayer.FightingTarget.Name != "Right Hand of Madness" &&
                                DynelManager.LocalPlayer.FightingTarget.Name != "Green Tower" &&
                                DynelManager.LocalPlayer.FightingTarget.Name != "Blue Tower" &&
                                DynelManager.LocalPlayer.FightingTarget.Name != "Xan Spirit of Redemption"*/

                {
                    DynelManager.LocalPlayer.StopAttack();
                    return true;
                }
            }
            return false;
        }
        private bool CheckFor12ManDebuffs()
        {
            try
            {

                if (DynelManager.LocalPlayer != null && DynelManager.LocalPlayer.FightingTarget != null && IsAttackingTeamMember())
                    Chat.WriteLine($"You are attacking {DynelManager.LocalPlayer.FightingTarget.Name}");

                foreach (Buff b in DynelManager.LocalPlayer.Buffs)
                {
                    if (b.Name == "False Redemption: Hai-Tempterus" && goToRed1 == false && goToRedPlat == false)
                    {
                        goToStartPointRed = true;
                        Chat.WriteLine($"Going to Red Pedestal");
                    }
                    if (b.Name == "False Redemption: Silvertail" && goToYellow1 == false && goToYellowPlat == false)
                    {
                        goToStartPointYellow = true;
                        Chat.WriteLine($"Going to Yellow Pedestal");
                    }
                    /*if (b.Name == "Saved from False Redemption" && goBack12Man1Yellow == false && goBack12Man1Red == false && goBack12Man2 == false
                         && goToYellow1 == false && goToYellow2 == false && goToYellowPlat == false
                         && goToRed1 == false && goToRed2 == false && goToRedPlat == false
                        )
                    {
                        resetMovementVars();
                        goBack12Man1Red = true;
                        Chat.WriteLine($"Going back");
                    }*/
                }
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.ToString());
            }
            return false;

        }

        private void CheckForAlienCoccoon()
        {
            try
            {
                if (DynelManager.LocalPlayer.Pets.Length == 0)
                    return;
                SimpleChar target = null;
                target = DynelManager.NPCs
                    .Where(c => c.Name == "Alien Coccoon" || c.Name == "Alien Cocoon")
                    .FirstOrDefault();

                if (target != null && target.IsAlive == true)
                {
                    DynelManager.LocalPlayer.Pets.Attack(target.Identity);
                    DynelManager.LocalPlayer.Attack(target.Identity);
                }
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.ToString());
            }

        }

        private void OnZoned(object s, EventArgs e)
        {
            resetMovementVars();
        }
        private bool CheckForPOHDebuffs()
        {
            foreach (Buff buff in DynelManager.LocalPlayer.Buffs)
            {
                if (buff.Name == "Call of Rust" || buff.Name == "Festering Skin")
                {
                    if (DynelManager.LocalPlayer.IsAttacking)
                        DynelManager.LocalPlayer.StopAttack();
                    
                    return true;
                }
            }

            foreach (TeamMember m in Team.Members)
            {
                if (m.Character != null && m.Character.Buffs != null)
                {
                    foreach (Buff b in m.Character.Buffs)
                    {
                        if (b.Name == "Call of Rust" || b.Name == "Festering Skin")
                            Chat.WriteLine($"{m.Name} has {b.Name}");
                    }
                }
            }
            return false;
        }
        private bool IsLocalPlayerOnAltar()
        {
            IEnumerable<SimpleChar> targets = DynelManager.Characters
                .Where(c => (c.Name == "Altar of Purification" || c.Name == "Altar of Torture"));
            foreach (SimpleChar altar in targets)
            {
                if (DynelManager.LocalPlayer.Position == altar.Position)
                    return true;
            }
                
            return false;
        }
        private SimpleChar FindPOHGreenAltar()
        {
            IEnumerable<SimpleChar> targets = DynelManager.Characters
                .Where(c => (c.Name == "Altar of Purification" || c.Name == "Altar of Torture"));

            foreach (SimpleChar altar in targets)
            {
                foreach (Buff buff in altar.Buffs)
                {
                    if (buff.Name == "Altar of Purification")
                    {
                        return altar;
                    }
                }

            }
            return null;
        }
        private bool goToLocation(Vector3 location)
        {
            bool arrived = false;

            float distanceToTarget = DynelManager.LocalPlayer.Position.DistanceFrom(location);

            if (distanceToTarget >= maxDistance)
            {
                if (!MovementController.Instance.IsNavigating)
                    MovementController.Instance.SetPath(new Path(location) { DestinationReachedDist = maxDistance });
            }
            else
            {
                MovementController.Instance.Update();
                arrived = true;
            }
            return arrived;
        }
        private void OnUpdate(object sender, float e)
        {
            try
            {
                CheckForAlienCoccoon();

                if (goBack12Man2 == true)
                {
                    bool arrived = goToLocation(fight_position_12man2);
                    if (arrived == true)
                    {
                        goBack12Man2 = false;
                    }
                }
                if (goBack12Man1Red == true)
                {
                    bool arrived = goToLocation(fight_position_12man1Red);
                    if (arrived == true)
                    {
                        goBack12Man1Red = false;
                        goBack12Man2 = true;
                    }
                }
                if (goBack12Man1Yellow == true)
                {
                    //DynelManager.LocalPlayer.Position = fight_position_12man1;
                    bool arrived = goToLocation(fight_position_12man1);
                    if (arrived == true)
                    {
                        goBack12Man1Yellow = false;
                        goBack12Man2 = true;
                    }
                }
                if (goToYellowPlat == true)
                {
                    bool arrived = goToLocation(yellow_pedestal_12man);
                    if (arrived == true)
                    {
                        goToYellowPlat = false;
                        goBack12Man1Yellow = true;

                    }
                }

                if (goToYellow2 == true)
                {
                    //DynelManager.LocalPlayer.Position = yellow_step2_12man;
                    bool arrived = goToLocation(yellow_step2_12man);
                    if (arrived == true)
                    {
                        goToYellow2 = false;
                        goToYellowPlat = true;
                    }
                }
                if (goToYellow1 == true)
                {
                    //DynelManager.LocalPlayer.Position = yellow_step1_12man;
                    bool arrived = goToLocation(yellow_step1_12man);
                    if (arrived == true)
                    {
                        goToYellow1 = false;
                        goToYellow2 = true;
                    }
                }
                if (goToStartPointYellow == true)
                {
                    bool arrived = goToLocation(startpoint_12man_yellow);
                    if (arrived == true)
                    {
                        goToStartPointYellow = false;
                        goToYellow1 = true;
                    }
                }
                if (goToRedPlat == true)
                {
                    bool arrived = goToLocation(red_pedestal_12man);
                    if (arrived == true)
                    {
                        goToRedPlat = false;
                        goBack12Man1Red = true;
                    }
                }
                if (goToRed1 == true)
                {
                   // DynelManager.LocalPlayer.Position = red_step1_12man;
                    bool arrived = goToLocation(red_step1_12man);
                    if (arrived == true)
                    {
                        goToRed1 = false;
                        goToRedPlat = true;
                    }
                }
                if (goToStartPointRed == true)
                {
                    bool arrived = goToLocation(startpoint_12man_red);
                    if (arrived == true)
                    {
                        goToStartPointRed = false;
                        goToRed1 = true;
                    }
                }

                if (testMeepToLocation1 == true)
                {
                    DynelManager.LocalPlayer.Position = location1meep;
                    testMeepToLocation1 = false;
                    testGoToLocation1 = true;
                }
                if (testGoToLocation1 == true)
                {
                    bool arrived = goToLocation(location1);
                    if (arrived == true)
                    {
                        testGoToLocation1 = false;
                    }
                }
                if (DynelManager.LocalPlayer.Profession == Profession.Keeper && castAntifear == true)
                {
                    //Courage of the Just is 279378

                    Spell.Find(279378, out Spell curSpell);
                    if (curSpell != null)
                    {
                        curSpell.Cast();
                    }
                }
                if (usePandeRing == true)
                {
                    usePandeRing = false;
                    if (Targeting.Target != null && Targeting.Target.Name == "Pandemonium Portal")
                    {
                        List<Item> characterItems = Inventory.Items;
                        foreach (Item item in characterItems)
                        {
                            if (item.Name.StartsWith("Pure Novictum Ring for the"))
                            {
                                Chat.WriteLine($"Using {item.Name}");
                                item.UseOn(Targeting.Target.Identity);
                                return;
                            }
                        }
                        
                        List<Container> backpacks = Inventory.Backpacks;
                        foreach (Container backpack in backpacks)
                        {
                            //Chat.WriteLine($"{backpack.Identity} - IsOpen:{backpack.IsOpen}{((backpack.IsOpen) ? $" - Items:{backpack.Items.Count}" : "")}");
                            Item noviRing;
                            if (backpack.Find(226290, out noviRing))
                            {
                                noviRing.UseOn(Targeting.Target.Identity);
                                return;
                            }
                            /*foreach (Item item in backpack.Items)
                            {
                                if (item.Name.StartsWith("Pure Novictum Ring for the"))
                                {
                                    Chat.WriteLine($"Using {item.Name}");
                                    item.UseOn(Targeting.Target.Identity);
                                    return;
                                }
                            }*/
                        }

                    }

                }
                if (monitor12manDebuff == true)
                {
                    CheckFor12ManDebuffs();
                }
                if (monitorPOHBuffs == true)
                {
                    if (needGreenLight == false)
                    {
                        needGreenLight = CheckForPOHDebuffs();
                    }

                    if (needGreenLight == true)
                    {
                        // Find an altar and meep there when the light is green
                        SimpleChar altar = FindPOHGreenAltar();
                        if (altar != null)
                        {
                            DynelManager.LocalPlayer.Position = altar.Position;
                            goToLocation(altar.Position);
                        }
                        needGreenLight = false;
                    }
                    if (positionProfsForFloor5 == true)
                    {
                        PositionProfsForFloor5();                        
                    }
                    if (needGreenLight == false && IsLocalPlayerOnAltar() == true)
                    { 
                        Floor5Attack();
                    }
                }
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.ToString());
            }
        }
        private void Floor5Attack()
        {
            SimpleChar target = null;
            switch (DynelManager.LocalPlayer.Profession)
            {
                case Profession.Doctor:
                case Profession.Trader:
                case Profession.Bureaucrat:
                case Profession.Engineer:
                case Profession.Soldier:
                case Profession.Enforcer:
                default:
                    // ok we are looking for (in that order)
                    // Sorrowful Voidling
                    // Fearful Voidling
                    // Pained Voidling

                    target = DynelManager.Characters
                        .Where(c => c.Name == "Sorrowful Voidling")
                        .FirstOrDefault();
                    if (target != null && (!DynelManager.LocalPlayer.IsAttacking || DynelManager.LocalPlayer.IsAttacking && DynelManager.LocalPlayer.FightingTarget.Name != "Sorrowful Voidling"))
                    {
                        DynelManager.LocalPlayer.Attack(target, true);
                        Chat.WriteLine("Fighting Sorrowful Voidling");
                        break;
                    }
                    else if (target == null)
                        target = DynelManager.Characters
                        .Where(c => c.Name == "Fearful Voidling")
                        .FirstOrDefault();
                    if (target != null && (!DynelManager.LocalPlayer.IsAttacking || DynelManager.LocalPlayer.IsAttacking && DynelManager.LocalPlayer.FightingTarget.Name != "Fearful Voidling"))
                    {
                        DynelManager.LocalPlayer.Attack(target, true);
                        Chat.WriteLine("Fighting Fearful Voidling");
                        break;
                    }

                    if (target == null)
                        target = DynelManager.Characters
                        .Where(c => c.Name == "Pained Voidling")
                        .FirstOrDefault();
                    if (target != null && (!DynelManager.LocalPlayer.IsAttacking || DynelManager.LocalPlayer.IsAttacking && DynelManager.LocalPlayer.FightingTarget.Name != "Pained Voidling"))
                    {
                        DynelManager.LocalPlayer.Attack(target, true);
                        Chat.WriteLine("Fighting Pained Voidling");
                        break;
                    }

                    target = DynelManager.Characters
                        .Where(c => c.Name == "Azdaja the Joyous")
                        .FirstOrDefault();
                    if ((target != null && (!DynelManager.LocalPlayer.IsAttacking) || (
                        DynelManager.LocalPlayer.IsAttacking && DynelManager.LocalPlayer.FightingTarget.Name != "Azdaja the Joyous")))
                        DynelManager.LocalPlayer.Attack(target, true);
                    break;
            }

        }
        private void PositionProfsForFloor5()
        {
            Vector3 pos;
            switch (DynelManager.LocalPlayer.Profession)
            {
                case Profession.Doctor:
                case Profession.Trader:
                case Profession.Bureaucrat:
                case Profession.Engineer:
                    pos = new Vector3(88.3, 1.0, 278.8);
                    break;
                case Profession.Enforcer:
                case Profession.Soldier:
                default:
                    pos = new Vector3(55.4, 1.0, 279.5);
                    break;
            }
            DynelManager.LocalPlayer.Position = pos;
            
            bool arrived = goToLocation(pos);

            if (arrived == true)
                positionProfsForFloor5 = false;
        }
        private void OnFloorMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning)
                return;
            // Stop syncing moves and attacks, we gonna prep for floor position in order to kill portal mobs or bosses
            syncMoves = false;
            syncAttacks = false;

            Vector3 lookAt;
            FloorMessage floorMsg = (FloorMessage)msg;

            floor = floorMsg.Floor;
            Chat.WriteLine($"Positioning for floor {floor}");
            switch (floor)
            {
                // Floor 1 there is nothing to do, just kill the two keeper mobs at entrance and go
                case 1:
                    break;
                // too easy for now to do anything, kill the portal mobs, you can actually kill first, and rush to kill second and the vortex boss will appear
                case 2:
                // Floor 3 separate tank + DD from support profs
                case 3:
                    switch (DynelManager.LocalPlayer.Profession)
                    {
                        case Profession.Enforcer:
                        case Profession.Soldier:
                        case Profession.Shade:
                        case Profession.MartialArtist:
                        case Profession.Keeper:
                            DynelManager.LocalPlayer.Position = floor2_tank_pos;
                            Chat.WriteLine($"Setting position for TANK spot on {floor}");

                            break;
                        case Profession.Doctor:
                        case Profession.Trader:
                        case Profession.Bureaucrat:
                        case Profession.Engineer:
                            DynelManager.LocalPlayer.Position = floor2_support_pos;

                            Chat.WriteLine($"Setting position for Support spot on {floor}");

                            break;

                    }
                    break;
                // Floor 4 "meep" 3 toons to the 3 portal mobs and kill them
                case 4:
                    switch (DynelManager.LocalPlayer.Profession)
                    {
                        case Profession.Soldier:
                        case Profession.MartialArtist:
                            // Put soldja or MA to the north position, just close enough so the door is open, but far enough so it gets no aggro
                            DynelManager.LocalPlayer.Position = new Vector3(70.9, 6.0f, 112.7);
                            lookAt = new Vector3(0, -0.9989639, 0);
                            movementController.AppendDestination(DynelManager.LocalPlayer.Position);

                            movementController.Update();
                            break;
                        case Profession.Engineer:
                            // Enfo on the west side
                            DynelManager.LocalPlayer.Position = new Vector3(58.3, 6.0f, 49.1);
                            break;
                        case Profession.Bureaucrat:
                            // Finally crat on East side.
                            DynelManager.LocalPlayer.Position = new Vector3(123.1, 6.0f, 46.6);
                            break;

                        default:
                            break;
                    }

                    //find a portal
                    IEnumerable<SimpleChar> targets = DynelManager.Characters
                        .Where(c => (c.Name == "Portal Warden"));

                    foreach (SimpleChar target in targets)
                    {
                        if (target.IsAlive == true)
                        {
                            target.Target();
                            DynelManager.LocalPlayer.Attack(target);
                            break;
                        }
                        else
                        {
                            Chat.WriteLine($"Found a ghost Portal Warden, skipping ...");
                        }
                    }
                    break;
                // Floor 5 serious shit starts here.
                case 5:
                    monitorPOHBuffs = true;
                    syncAttacks = true;
                    positionProfsForFloor5 = true;

                    break;

            }
            movementController.Update();

        }
    }


    public class Utils
    {
        public static int Next(int min, int max)
        {
            if (min >= max)
            {
                throw new ArgumentException("Min value is greater or equals than Max value.");
            }

            byte[] intBytes = new byte[4];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(intBytes);
            }

            return min + Math.Abs(BitConverter.ToInt32(intBytes, 0)) % (max - min + 1);
        }
    }

}
