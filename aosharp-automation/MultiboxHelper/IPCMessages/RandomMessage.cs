﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace MultiboxHelper.IPCMessages
{
    [AoContract((int)IPCOpcode.Random)]
    public class RandomMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Random;


        [AoMember(1)]
        public int PlayfieldId { get; set; }

        [AoMember(2)]
        public int command { get; set; }

    }
}
