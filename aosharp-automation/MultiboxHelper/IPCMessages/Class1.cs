﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using WindowsInput.Native;
using WindowsInput;

namespace MultiboxHelper.IPCMessages
{
    [AoContract((int)IPCOpcode.Dance)]
    public class DanceMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.Dance;

        [AoMember(0)]
        public InputSimulator dance { get; set; }
    }
}