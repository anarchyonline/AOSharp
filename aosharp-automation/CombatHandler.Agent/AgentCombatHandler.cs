﻿
using System.Linq;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI.Options;
using CombatHandler.Generic;
using System.Collections.Generic;
using AOSharp.Core.UI;
using System;

namespace Desu
{
    public class AgentCombatHandler : GenericCombatHandler
    {
        private Menu _menu;

        public AgentCombatHandler()
        {
            //Perks

            RegisterPerkProcessor(PerkHash.ArmorPiercingShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.ConcussiveShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.FindTheFlaw, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.TheShot, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.DeathStrike, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.PinpointStrike, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.SoftenUp, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Assassinate, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Tranquilizer, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.ShadowBullet, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.NightKiller, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.SilentPlague, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Recalibrate, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.Fuzz, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.FireFrenzy, TargetedDamagePerk);
            RegisterPerkProcessor(PerkHash.LEProcAgentLaserAim, LEProc);
            RegisterPerkProcessor(PerkHash.LEProcAgentGrimReaper,LEProc);




            RegisterSpellProcessor(RelevantNanos.AssassinGrin, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.GnatsWIng, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.Phase4, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.AssassinAimedShot, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.LesserPredator, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.WayofTheExecutioner, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompleteHealing, CompleteHeal);
            RegisterSpellProcessor(RelevantNanos.LifegivingElixir, LifegivingElixir);
            RegisterSpellProcessor(RelevantNanos.FpDoc, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ImprovedInstinctiveControl, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ImprovedNanoRepulsor, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.ContinuousReconstruction, GenericBuff);
            //RegisterSpellProcessor(RelevantNanos.EnhancedSureshot, TeamBuff);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AgilityBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AimedShotBuffs).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ConcealmentBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.CriticalIncreaseBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ExecutionerBuff).OrderByStackingOrder(), GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.RifleBuffs).OrderByStackingOrder(), RifleBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.AgentProcBuff).OrderByStackingOrder(), GenericBuff);
            //RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.ConcentrationCriticalLine).OrderByStackingOrder(), Concentration, CombatActionPriority.Medium);

            //RegisterSpellProcessor(RelevantNanos.HEALS, Healing, CombatActionPriority.Medium);
            RegisterSpellProcessor(RelevantNanos.CH, CompleteHeal, CombatActionPriority.High);



            // RegisterSpellProcessor(RelevantNanos.SuperiorOmniMedEnhancement, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.LifeChanneler, GenericBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.CompleteHealingLine).OrderByStackingOrder(), CompleteHeal);







            //Spells
            RegisterSpellProcessor(RelevantNanos.Ubt, Ubt, CombatActionPriority.Low);
            RegisterSpellProcessor(RelevantNanos.Bullseye, AgentDebuff, CombatActionPriority.Low);
            RegisterSpellProcessor(RelevantNanos.AbsoluteConcentration, AgentDebuff, CombatActionPriority.Low);



            _menu = new Menu("CombatHandler.Agent", "CombatHandler.Agent");
            _menu.AddItem(new MenuBool("UseDebuff", "Agent Debuffing", true));
            _menu.AddItem(new MenuBool("UseUBT", "Agent Ubting", true));
            _menu.AddItem(new MenuBool("BuffTeamMembers", "BuffTeamMembers?", false));

            OptionPanel.AddMenu(_menu);

        }

        private bool RifleBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Buffs.Find(RelevantNanos.AssassinsAimedShot, out Buff AAS)) { return false; }

            if (DynelManager.LocalPlayer.Buffs.Find(RelevantNanos.SteadyNerves, out Buff SN)) { return false; }

            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }


        private bool AgentDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)

        {
            // Check if we are fighting and if debuffing is enabled
            if (fightingTarget == null || !_menu.GetBool("UseDebuff"))
                return false;
            //Check the remaining time on debuffs. On the enemy target
            foreach (Buff buff in fightingTarget.Buffs.AsEnumerable())
            {
                //Chat.WriteLine(buff.Name);
                if (buff.Name == spell.Name && buff.RemainingTime > 1)
                    return false;

            }

            return true;
        }
        private bool Ubt(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)

        {
            // Check if we are fighting and if debuffing is enabled
            if (fightingTarget == null || !_menu.GetBool("UseUBT"))
                return false;
            //Check the remaining time on debuffs. On the enemy target
            foreach (Buff buff in fightingTarget.Buffs.AsEnumerable())
            {
                //Chat.WriteLine(buff.Name);
                if (buff.Name == spell.Name && buff.RemainingTime > 1)
                    return false;

            }

            return true;
        }


        private bool CompleteHeal(Spell spell, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 40)
            {
                actiontarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.IsAlive)
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent < 40)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();

                if (DynelManager.LocalPlayer.NanoPercent < 20)

                    return false;

                else if (dyingTeamMember != null)
                {
                    actiontarget.Target = dyingTeamMember;

                    return true;
                }
            }

            return false;
        }

        private bool LifegivingElixir(Spell spell, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            // Prioritize keeping ourself alive
            if (DynelManager.LocalPlayer.HealthPercent <= 85)
            {
                actiontarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            // Try to keep our teammates alive if we're in a team
            if (DynelManager.LocalPlayer.IsInTeam())
            {
                SimpleChar dyingTeamMember = DynelManager.Characters
                    .Where(c => c.IsAlive)
                    .Where(c => Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                    .Where(c => c.HealthPercent < 85)
                    .OrderByDescending(c => c.GetStat(Stat.NumFightingOpponents))
                    .FirstOrDefault();
              
                if (dyingTeamMember != null)
                {
                    actiontarget.Target = dyingTeamMember;
                    Debug.DrawLine(DynelManager.LocalPlayer.Position, dyingTeamMember.Position, DebuggingColor.Red);
                    return true;
                }
            }

            return false;
        }

        private static class RelevantNanos
        {
            public static int[] DetauntProcs = { 226437, 226435, 226433, 226431, 226429, 226427 };
            public static int[] FalseProfDoc = { 117210, 117221, 32033 };
            public static int[] FalseProfEng = { 117213, 117224, 32034 };
            public static int[] FalseProfSol = { 117216, 117227, 32038 };
            public static int[] FalseProfCrat = { 117209, 117220, 32032 };
            public static int[] FalseProfTrader = { 117211, 117222, 32040 };
            public static int[] FalseProfAdv = { 117214, 117225, 32030 };
            public static int[] FalseProfMp = { 117210, 117221, 32033 };
            public static int[] FalseProfFixer = { 117212, 117223, 32039 };
            public static int[] FalseProfEnf = { 117217, 117228, 32041 };
            public static int[] FalseProfMa = { 117215, 117226, 32035 };
            public static int[] FalseProfNt = { 117207, 117218, 32037 };
            public static int[] DotProcs = { 226425, 226423, 226421, 226419, 226417, 226415, 226413, 226410 };
            public static int[] TeamCritBuffs = { 160791, 160789, 160787 };
            public static int AssassinsAimedShot = 275007;
            public static int SteadyNerves = 160795;
            public static int CH = 28650;
            public static int TeamCH = 42409; //Add logic later
            public const int TiredLimbs = 99578;
            public static int[] HEALS = new[] { 223299, 223297, 223295, 223293, 223291, 223289, 223287, 223285, 223281, 43878, 43881, 43886, 43885,
                43887, 43890, 43884, 43808, 43888, 43889, 43883, 43811, 43809, 43810, 28645, 43816, 43817, 43825, 43815,
                43814, 43821, 43820, 28648, 43812, 43824, 43822, 43819, 43818, 43823, 28677, 43813, 43826, 43838, 43835,
                28672, 43836, 28676, 43827, 43834, 28681, 43837, 43833, 43830, 43828, 28654, 43831, 43829, 43832, 28665 };


            public const int Ubt = 99577;
            public const int Bullseye = 275823;
            public const int CompleteHealing = 28650;
            public const int LifegivingElixir = 43878;



            // Buffs

            public static readonly int FpDoc = 117210;
            public static readonly int WayofTheExecutioner = 275822;
            public static readonly int AssassinGrin = 81856;
            public static readonly int GnatsWIng = 273293;
            public static readonly int Phase4 = 273296;
            public static readonly int AssassinAimedShot = 275007;
            public static readonly int LesserPredator = 263240;
            public static readonly int AbsoluteConcentration = 160712;
            public static readonly int ImprovedInstinctiveControl = 222856;
            public static readonly int ImprovedNanoRepulsor = 222823;
            public static readonly int ContinuousReconstruction = 222824;
            public static readonly int SuperiorOmniMedEnhancement = 95709;
            public static readonly int LifeChanneler = 96247;
            public static readonly int EnhancedSureshot = 160791;

        }
    }
}


