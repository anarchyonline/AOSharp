[system.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | out-null


function Click-MouseButton
{
    param([switch] $reverse)

    $array = import-csv $psscriptroot\clicks.csv -Delimiter ';'

    $clicks = @()
    if ($reverse.IsPresent)
    {
        for ($i = $array.count -1; $i -ge 0; $i--)
        {
            $clicks += $array[$i]
        }        
    }
    else
    {
        $clicks = $array
    }
    $signature=@' 
      [DllImport("user32.dll",CharSet=CharSet.Auto, CallingConvention=CallingConvention.StdCall)]
      public static extern void mouse_event(long dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);
'@ 

    $SendMouseClick = Add-Type -memberDefinition $signature -name "Win32MouseEventNew" -namespace Win32Functions -passThru 

    foreach ($click in $clicks)
    {
        [System.Windows.Forms.Cursor]::Position = New-Object System.Drawing.Point($click.x, $click.y)
        $SendMouseClick::mouse_event(0x00000002, 0, 0, 0, 0)
        $SendMouseClick::mouse_event(0x00000004, 0, 0, 0, 0)
        sleep -Milliseconds 300
    }
}

function Register-MouseButton
{
    $array = @()

    while($true)
    {
        Write-Host -NoNewLine 'Press any key to continue...';
        $key = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
        $key
        if ($key -and $key.Character -eq "x")
        {
            $array | select x, y | Export-csv $psscriptroot\clicks.csv -NoTypeInformation -Delimiter ';'
            return
        }
        $o = [pscustomobject] @{
            x = [System.Windows.Forms.Cursor]::Position.X
            y = [System.Windows.Forms.Cursor]::Position.Y
        }
        $array += $o
    }
}
