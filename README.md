This project is a fork from https://gitlab.com/never-knows-best

The project structure is unchanged, namely:

- aosharp

Core project, it's what's links AOSharp to AO. 

- aosharp.automation

Profession combat handlers (CH) 
Here you will find all the classes for the various professions. These classes have been enhanced from the original project. Crats casts pets, just like engis, buff them, and if you are in a team every member will buff everyone and keep them buffed, ensure you have plenty of NCUs.

- aosharp.bots

This section contains the bots.
You still have your favorite Infbuddy and Apfbuddy bots, but you also have Reform to quickly create and reform teams (handy to exit APF/POH)
Dummybot has been renamed to Autoattack because it's not that dummy anymore. Autoattack has a KOS list useful in places like S13, for example. It also has an ignore list that has been updated with NPCs from all sectors.


This version of AOSharp focuses on commands instead of menu buttons on each client:

1. Reform

**/reform** - reforms the team, invites are automatically sent and accepted

2. MultiboxHelper

**/engi nsd** - makes the engi in the team cast NSD
**/engi blinds** - makes the engi in the team cast blinds
**/syncattacks** (or /sa) [on/off] or [true/false] or [start/stop]
**/syncmoves** (or /sm) [on/off] or [true/false] or [start/stop]
**/sa** and **/sm** commands without parameter enables them
**/floor** <number> POH work in progress not yet fully functional

4. Autoattack

**/auto** [on/off] [start/stop] [true/false]


